package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

const (
	appName    = "kvgen"
	appDesc    = "Generates code for dedicated kv stores"
	appVersion = "0.0.0"
	appOwner   = "scaluch@gmail.com"
)

var (
	flags = []cli.Flag{
		cli.StringFlag{
			Name:  "type",
			Usage: "the type of the entities going in and out of the store",
		},
		cli.StringFlag{
			Name:  "log-level",
			Usage: "the log level",
			Value: "trace",
		},
	}
)

const templateSuffix = ".gogo"

func main() {
	app := cli.NewApp()
	app.Name = appName
	app.Copyright = appOwner
	app.Version = appVersion
	app.Description = appDesc
	app.Flags = flags
	app.Action = func(c *cli.Context) error {
		logrus.Debugln("starting:")
		ll, err := logrus.ParseLevel(c.String("log-level"))
		if nil != err {
			return err
		}
		logrus.SetLevel(ll)

		return nil
	}

	if err := app.Run(os.Args); err != nil {
		logrus.WithError(err).Fatal("Generator terminated.")
	}
}
